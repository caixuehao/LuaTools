
local read_all_file = nil
read_all_file = function(root_path,func,limit_names)
    for file_name in lfs.dir(root_path) do
        if file_name ~= "." and file_name ~= ".." then 
            local file_path = root_path.. '/' ..file_name
            local attr = lfs.attributes (file_path)
            if attr then
                if attr.mode == "directory" then
                    read_all_file(file_path,func,limit_names)
                else
                    local full_name, file_name,extension_name,path = string.get_file_info(file_path)
                    if limit_names==nil or limit_names[extension_name] then
                        func(file_path,full_name,extension_name)
                    end
                end
            end
        end
    end
end


local mian = function()

    -- --例子：把所有.lua .txt 中的 "self:AddClick(****," 替换为 "self:AddClick(****.gameObject,"   并且加上空格换行，重复gameObject 容错
    -- read_all_file("C:/Users/Administrator/Desktop/test",function(path,full_name,extension_name)
    --     print(full_name)
    --     local file = io.open(path,"r")
    --     if file then
    --         local file_data = file:read("*a")
    --         local out_data = string.gsub(file_data,"(self:%s*AddClick%s*%(%s*([^\n]*)%s*,)",function(a,b)
    --             if string.sub(b,-11)==".gameObject" then
    --                 return a
    --             else
    --                 return "self:AddClick("..b..".gameObject"..","
    --             end
    --         end)
    --         local out_file = io.open(path,"w")
    --         out_file:write(out_data)
    --         out_file:close()
    --     end
    --     file:close()
    -- end,{[".lua"]=true,[".txt"]=true})

    --例子：给文件里的每一行都加上 双引号
    local path = "C:/Users/Administrator/Desktop/LaunchPanel.txt"
    local file = io.open(path,"r")
    if file  then
        local line_datas = {}
        for i=1,100000 do
            local line_data = file:read("*l")
            if line_data then
                line_data = string.format("\"%s\",\n",line_data)
                table.insert(line_datas,line_data)
            else
                break;
            end
        end
        local out_file = io.open(path,"w")
        for _,data in ipairs(line_datas) do
            out_file:write(data)
        end
        out_file:close()
    end
    file:close()

end


return mian