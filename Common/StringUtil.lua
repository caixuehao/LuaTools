--https://www.cnblogs.com/meamin9/p/4502461.html 正则表达式
--https://www.jianshu.com/p/be7fa619bb44 --lua 获取UTF-8字符串编码


-- Unicode符号范围	UTF-8字节数	 UTF-8编码方式（二进制）
--  (0-127)	        1	        0xxxxxxx
--  (128-2047)	    2	        110xxxxx 10xxxxxx
--  (2048-65535)	3	        1110xxxx 10xxxxxx 10xxxxxx
--  (65536-1050623)	4	        11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
--分割UTF8字符串获取字符列表
local function utf8charlist(str)
    if str==nil or string.len(str)<=0 then
		return {}
	end
	local charlist = {}
    local tmpcharnum = {}
	for i = 1,string.len(str),1 do
		local num = string.byte(str,i)
		if num<=127 or num>=192 then
			if #tmpcharnum>0 then
				table.insert( charlist,string.char(unpack(tmpcharnum,1,#tmpcharnum)))
				tmpcharnum = {}
			end
		end
		if num<=127 then
			table.insert( charlist,string.char(num))
		elseif num<=191 then
			table.insert( tmpcharnum,num)
		elseif num<=255 then
			table.insert( tmpcharnum,num)
		end
	end
	return charlist
end

--分割GBK字符串获取字符列表(非ASCII字符两个字节)
local function gbkcharlist(str)
    if str==nil or string.len(str)<=0 then
		return {}
    end
    local charlist = {}
    local i = 1
    local len = string.len(str)
    while (i<=len) do
        local num = string.byte(str,i)
        if num<=127 then
            table.insert( charlist,string.char(num))
            i = i + 1
        else
            table.insert( charlist,string.char(num,string.byte(str,i+1)))
            i = i + 2
        end
    end
    return charlist
end

--返回 文件名+后缀名，文件名，后缀名，路径
local function get_file_info(full_path)
	if full_path==nil or full_path=="" or type(full_path)~="string" then
		return nil
	end
	local re_path = string.reverse(full_path)
	local path,full_name, file_name,extension_name
	local index = string.find(re_path,"/")
	if index then
		path = string.sub(re_path,index+1)
		full_name = string.sub(re_path,1,index-1)
	else
		path = nil;full_name = re_path
	end
	if full_name then
		index = string.find(full_name,"%.")
		if index then
			file_name = string.sub(full_name,index+1)
			extension_name = string.sub(full_name,1,index)
		else
			file_name = full_path
			extension_name = nil
		end
	end
	return string.reverse(full_name),string.reverse(file_name),string.reverse(extension_name),string.reverse(path)
end

--原生方法
--string.sub(str,起始位置,结束位置) -- 获取指定位置长度的字符串
--string.upper(argument)    --字符串全部转为大写字母。
--string.lower(argument)    --字符串全部转为小写字母。
--..                        --链接两个字符串
--string.len(arg)           --计算字符串长度。
--string.rep(string, n)     --返回字符串string的n个拷贝 string.rep("abcd",2) -> abcdabcd
--string.format(...)        --格式化字符串
--string.reverse(arg)       --字符串反转 string.reverse("Lua") -> auL
--string.find (str, substr, [init, [end]])     --在一个指定的目标字符串中搜索指定的内容 返回其具体位置。不存在则返回nil string.find("test test test","test",2) -> 6 9
--string.char(...)                              --将整型数字转成字符并连接 string.char(68,67,68) -> DCD
--string.byte(arg[,int])                        --转换字符为整数值 string.byte("ABC",2) -> 66
--[[string.gsub(s, pattern, repl[, n])  
	--在字符串中替换。替换次数 n==nil 时替换全部 string.gsub("aaaa","a","z",3) -> zzza
	--参数repl可以是正则表达式，也可以是函数。当repl是函数时，函数的参数是模式pattern捕获的子串，和match类似，有分组返回分组，无分组返回整个子串。函数最后应该返回一个字符串。如果repl是正则表达式，可以用分组序号引用匹配到的分组。
]]
--[[string.gmatch(str, pattern)                              
回一个迭代器函数，每一次调用这个函数，返回一个在字符串 str 找到的下一个符合 pattern 描述的子串。如果参数 pattern 描述的字符串没有找到，迭代函数返回nil。
for word in string.gmatch("Hello Lua user", "%a+") do
     print(word) 
end
->Hello Lua user
]]
--[[string.match(str, pattern, init)
string.match()只寻找源字串str中的第一个配对. 参数init可选, 指定搜寻过程的起点, 默认为1。 
在成功配对时, 函数将返回配对表达式中的所有捕获结果; 如果没有设置捕获标记, 则返回整个配对字符串. 当没有成功的配对时, 返回nil。
string.format("%d, %q", string.match("I have 2 questions for you.", "(%d+) (%a+)"))
->2, "questions"
]]
string.utf8charlist = utf8charlist
string.gbkcharlist = gbkcharlist
string.get_file_info = get_file_info