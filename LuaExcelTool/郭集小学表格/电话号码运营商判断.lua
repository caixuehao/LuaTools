
--电话运营商判断
local Operator = function (str)
        if string.len( str )~=11 then
            return "电话号码填写错误，不满11位"
        end
        -- 移动的号段
        local type1 = { "134", "135", "136", "137", "138", "139", "150",
				"151", "152", "158", "159", "182", "183", "184", "157", "187",
				"188", "147", "178" };
		--联通的号段
		local type2 = { "130", "131", "132", "155", "156", "145", "185",
				"186", "176", "175" , "171"};
		--电信的号段
		local type3 = { "133","149","173", "153", "180", "181", "189", "177" };
        local head = string.sub( str, 1,3 ) 
        for _,h in pairs(type1) do
            if h==head then
                return "移动"
            end
        end
        for _,h in pairs(type2) do
            if h==head then
                return "联通"
            end
        end
        for _,h in pairs(type3) do
            if h==head then
                return "电信"
            end
        end
        return "未知"
end


return function (excel)
    --定义变量
    local data_path = "C:\\Users\\Administrator\\Desktop\\无wifi.xlsx"
    local DATA_START = 3
    local DATA_END = 100
    --逻辑
    local data_book =excel.Workbooks:Open(data_path);
    local data_sheet = data_book.Sheets(1);
    local names = {}
    for i=DATA_START,DATA_END do
        if data_sheet.Cells(i, 5).Value2 then
            local name = data_sheet.Cells(i, 1).Value2
            if name then
                if names[name] then
                    data_sheet.Cells(i, 8).Value2 = "重复"
                else
                    names[name] = true
                end
            end
            data_sheet.Cells(i, 6).Value2 = Operator(tostring(data_sheet.Cells(i, 5).Value2))
        else
            break;
        end
    end
    data_book:Save()
    data_book:Close()
end