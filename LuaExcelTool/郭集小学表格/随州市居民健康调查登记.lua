
return function (excel)
    --定义变量
    local data_path = "C:\\Users\\Administrator\\Desktop\\随州市居民健康调查登记表1(2)(2).xlsx"
    local out_path = "C:\\Users\\Administrator\\Desktop\\9组.xlsx"
    local DI_ZHI = "郭集村九组"
    local DATA_MAX = 3000--查找到第几行。
    --准备原始数据
    local book =excel.Workbooks:Open(data_path);
    local sheet = book.Sheets(1);
    local guoji8 = {}
    local is_new_home = true
    local index = 0
    for i=1,DATA_MAX do
        local diZhi = tostring(sheet.Cells(i, 2).Value2)

        if DI_ZHI==diZhi then
            if is_new_home==true then
                index = index + 1
                guoji8[index] = {names = {},ids = {},dianhua = nil,diZhi=nil}
                is_new_home= false
            end
            table.insert( guoji8[index].names,tostring(sheet.Cells(i, 1).Value2))
            guoji8[index].diZhi = diZhi
            print((sheet.Cells(i, 1).Value2))
            table.insert( guoji8[index].ids,tostring(sheet.Cells(i, 3).Value2))
            if sheet.Cells(i, 4).Value2~=nil then
                guoji8[index].dianhua = "'"..(sheet.Cells(i,4).Value2)
            end
        else
            is_new_home=true
        end
    end
    book:Close();

    --输出数据
    local book2 =excel.Workbooks:Add();
    local sheet2 = book2.Sheets(1);
    local out_index = 1
    for i=1,#guoji8 do
        local home = guoji8[i]
        local home_start_index = out_index
        local len = #home.names
        print(len)
        sheet2.Cells(out_index,1).Value2 = i
        for ii=1,len do
            for iii=1,len do
                if iii~=ii or len==1 then
                    sheet2.Cells(out_index,2).Value2 = home.names[ii]
                    sheet2.Cells(out_index,3).Value2 = home.diZhi 
                    sheet2.Cells(out_index,4).Value2 = "'"..(home.ids[ii])
                    sheet2.Cells(out_index,5).Value2 = (home.dianhua) or "'"
                    sheet2.Cells(out_index,6).Value2 = "1"
                    sheet2.Cells(out_index,7).Value2 = home.names[iii]
                    sheet2.Cells(out_index,8).Value2 = "'"..(home.ids[iii])
                    sheet2.Cells(out_index,9).Value2 = (home.dianhua) or "'"
                    sheet2.Cells(out_index,10).Value2 = "1"
                    sheet2.Cells(out_index,11).Value2 = "1"
                    out_index = out_index + 1
                end
            end
        end
        sheet2:Range(string.format( "A%d:A%d",home_start_index,out_index-1)):Merge()--合并单元格
    end
    book2:SaveAs(out_path);
    book2:Close()
end

