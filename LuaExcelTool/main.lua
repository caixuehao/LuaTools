---变量定义区
local LuaName= "DOLO/DicToExcel"--要运行的lua
---------------
package.path = package.path..";../?.lua"
require("lfs")
require("luacom")
require("Common/TableUtil")

local main = function ()
    --创建excel对象
    local excel = luacom.CreateObject("Excel.Application"); --创建新的实例
    excel.Visible = false;--不显示窗口
    excel.DisplaylogicAlerts = false--不显示关闭窗口
    --逻辑
    require(LuaName)(excel)
    --关闭excel
    excel:Quit();
    excel = nil
end


main()
main = nil
collectgarbage("collect");--启用一次完整的垃圾回收